package com.clevertec.study.cluster.service;

import com.clevertec.study.cluster.dto.Employee;

import java.util.Optional;

public interface EmployeeService {
    Optional<Long> saveEmplyee(Employee employee);
    Optional<Employee> getEmployeeById(Long id);
}
