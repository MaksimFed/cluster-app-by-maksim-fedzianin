package com.clevertec.study.cluster.dto;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class EmployeePosition {

    @Id
    private Long id;
    private String title;
    private Integer salary;
}
