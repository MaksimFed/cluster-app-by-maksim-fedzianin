package com.clevertec.study.cluster.service.impl;

import com.clevertec.study.cluster.dto.Employee;
import com.clevertec.study.cluster.repository.EmployeeRepository;
import com.clevertec.study.cluster.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService{

    private final EmployeeRepository employeeRepository;

    @Override
    public Optional<Long> saveEmplyee(Employee employee) {
        //return null;
        return Optional.of(employeeRepository.save(employee).getId());
    }

    @Override
    public Optional<Employee> getEmployeeById(Long id) {
        return employeeRepository.findById(id);
        //return null;
    }
}
