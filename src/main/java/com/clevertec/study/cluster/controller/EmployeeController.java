package com.clevertec.study.cluster.controller;

import com.clevertec.study.cluster.dto.Employee;
import com.clevertec.study.cluster.repository.EmployeeRepository;
import com.clevertec.study.cluster.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
@Slf4j
public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<?> addEmployee(@RequestBody Employee employee) {
        log.info("addEmployee() called, employee: {}", employee);
        //return ResponseEntity.ok(0);
        return ResponseEntity.ok(employeeService.saveEmplyee(employee));
    }


}
